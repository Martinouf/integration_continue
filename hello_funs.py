""" hello_funs is a set of function that can be used to say stuffs  """

def say_something_to_someone(something, someone):
	return something+" "+someone

def say_hello(to_who):
	return say_something_to_someone("hello",to_who)

def say_happy_new_year(to_who):
	return say_something_to_someone("happy new year",to_who)

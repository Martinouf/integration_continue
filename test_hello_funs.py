""" unit tests for hello_funs """
import unittest
import hello_funs

class HelloFunsTests(unittest.TestCase):
	""" test suit for HelloFunsTests """

	def test_say_something_to_someone(self):
		self.assertEqual(hello_funs.say_something_to_someone("Yo","Bob"), "Yo Bob")
		self.assertEqual(hello_funs.say_something_to_someone("",""), " ")


	def test_say_hello(self):
		self.assertEqual(hello_funs.say_hello("Bob"), "hello Bob")
		self.assertEqual(hello_funs.say_hello(""), "hello ")

	def test_say_happy_new_year(self):
		self.assertEqual(hello_funs.say_happy_new_year("Bob"), "happy new year Bob")
		self.assertEqual(hello_funs.say_happy_new_year(""), "happy new year ")

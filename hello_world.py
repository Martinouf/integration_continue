""" hello_world is a very simple set of tests for hello_funs """

import hello_funs

print(hello_funs.say_something_to_someone("Yo","Bob"))
print(hello_funs.say_hello("Uncle Bob"))
print(hello_funs.say_happy_new_year("Uncle Bob"))
